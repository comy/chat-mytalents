import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class ChatService {
  constructor(private db: AngularFirestore, private firebaseAuth: AngularFireAuth) {
  }

  sendMessage(pMessage: string, personID: string) {
    this.db
    .collection('profiles')
    .doc(this.firebaseAuth.auth.currentUser.uid)
    .collection('chats')
    .doc(personID)
    .collection('messages')
    .add({
        message: pMessage,
        createAt: new Date(),
        owner: this.firebaseAuth.auth.currentUser.uid,
        member: personID
    });

    this.db
    .collection('profiles')
    .doc(personID)
    .collection('chats')
    .doc(this.firebaseAuth.auth.currentUser.uid)
    .collection('messages')
    .add({
        message: pMessage,
        createAt: new Date(),
        owner: this.firebaseAuth.auth.currentUser.uid,
        member: personID
    });
  }

  getMessagesWithPerson(personID: string) {
    return this.db
    .collection('profiles')
    .doc(this.firebaseAuth.auth.currentUser.uid)
    .collection('chats')
    .doc(personID)
    .collection('messages', ref => ref.orderBy('createAt'))
    .valueChanges();
  }

  getMessagesFromOpenRoom() {
      return this.db
      .collection('chatRooms')
      .doc('openRoom')
      .collection('messages', ref => ref.orderBy('createAt'))
      .valueChanges();
  }

  sendMessageToOpenRoom(pMessage: string) {
    this.db
    .collection('chatRooms')
    .doc('openRoom')
    .collection('messages')
    .add({
        message: pMessage,
        createAt: new Date(),
        owner: this.firebaseAuth.auth.currentUser.uid
    });
  }

}
