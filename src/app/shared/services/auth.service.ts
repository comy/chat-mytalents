import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user: Observable<firebase.User>;

  constructor(private firebaseAuth: AngularFireAuth, private router: Router, private db: AngularFirestore) {
    this.user = firebaseAuth.authState;
  }

  getCurrentUser() {
    return new Promise<any>((resolve, reject) => {
      const user = firebase.auth().onAuthStateChanged((pUser) => {
        if (pUser) {
          resolve(pUser);
        } else {
          reject('Benutzer nicht angemeldet.');
        }
      });
    });
  }

  login(email: string, password: string) {
    return this.firebaseAuth
      .auth
      .signInWithEmailAndPassword(email, password)
      .then(value => {
        if (value.user.emailVerified) {
          return {success: true, message: 'Erfolgreich angemeldet'};
        } else {
          this.firebaseAuth.auth.signOut();
          this.user = null;
          return {success: false, message: 'E-Mail Adresse bitte bestätigen'};
        }
      })
      .catch(err => {
        console.log('Etwas ist schief gelaufen:', err.message);
        return {success: false, message: 'E-Mail Adresse oder Passwort falsch'};
      });
  }

  register(pEmail: string, pPassword: string, pUsername: string) {
    return this.firebaseAuth
      .auth
      .createUserWithEmailAndPassword(pEmail, pPassword)
      .then(value => {
        return this.db
        .collection('profiles')
        .doc(value.user.uid)
        .set({
          ID: value.user.uid,
          username: pUsername,
          email: pEmail
        }).then(() => {
          this.firebaseAuth.auth.currentUser.sendEmailVerification();
          this.firebaseAuth.auth.signOut();
          this.user = null;
          return {success: true, message: 'E-Mail Adresse bestätigen'};
        }).catch((error) => {
          console.log(error);
        });
      })
      .catch(err => {
        console.log('Etwas ist schief gelaufen:', err.message);
        return {success: false, message: 'Etwas ist schief gelaufen'};
    });
  }

  logout() {
    this.firebaseAuth
      .auth
      .signOut()
    .then(() => {
      this.router.navigate(['/login']);
    })
    .catch((error) => {
      console.log('logout error: ', error);
    });
  }

}
