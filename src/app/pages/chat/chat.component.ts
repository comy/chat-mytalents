import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';
import { ChatService } from 'src/app/shared/services/chat.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { UserService } from 'src/app/shared/services/user.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {
  mitgliederList: Observable<any[]>;
  messageInput: string;
  chatConversation: Observable<any[]>;
  windowID: string; // windowID is openRoom (room with all persons) or personID (one person)
  constructor(private firebaseAuth: AngularFireAuth, private db: AngularFirestore, private chatService: ChatService, public authService: AuthService, private userService: UserService) {
    this.mitgliederList = this.userService.getAllProfiles();
    this.windowID = 'openRoom';
  }

  ngOnInit() {
    this.showWindow('openRoom');
  }

  sendMSG() {
    if (this.windowID  === 'openRoom') {
      this.chatService.sendMessageToOpenRoom(this.messageInput);
    } else {
      this.chatService.sendMessage(this.messageInput, this.windowID);
    }
    this.messageInput = '';
  }

  showWindow(pWindowID: string) {
    this.windowID = pWindowID;
    if (this.windowID  === 'openRoom') {
      this.chatConversation = this.chatService.getMessagesFromOpenRoom();
    } else {
      this.chatConversation = this.chatService.getMessagesWithPerson(this.windowID);
    }
  }

}
