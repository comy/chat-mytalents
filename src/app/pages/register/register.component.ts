import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/shared/services/auth.service';
import { AlertsService } from 'angular-alert-module';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  username: string;
  email: string;
  password: string;
  constructor(private authService: AuthService, private alerts: AlertsService,  private router: Router) { }

  ngOnInit() {
    return new Promise((resolve, reject) => {
      this.authService.getCurrentUser()
        .then(user => {
          this.router.navigate(['/chat']);
          return resolve(true);
        }, err => {
          return resolve(false);
        });
    });
  }

  register() {
    this.authService
    .register(this.email, this.password, this.username)
    .then((response: any) => {
      console.log(response);
      if (response.success) {
        this.alerts.setMessage(response.message, 'warn');
        this.router.navigate(['/login']);
      } else {
        this.alerts.setMessage(response.message, 'error');
      }
    }).catch((error) => {
      this.alerts.setMessage('Etwas ist schief gelaufen', 'error');
    });
  }

}
