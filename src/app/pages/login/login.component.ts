import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../shared/services/auth.service';
import { AlertsService } from 'angular-alert-module';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email: string;
  password: string;
  constructor(private authService: AuthService, private alerts: AlertsService, private router: Router) { }

  ngOnInit() {
    return new Promise((resolve, reject) => {
      this.authService.getCurrentUser()
        .then(user => {
          this.router.navigate(['/chat']);
          return resolve(true);
        }, err => {
          return resolve(false);
        });
    });
    this.alerts.setDefaults('timeout', 5);
  }

  login() {
    this.authService.login(this.email, this.password).then((response) => {
      if (response.success) {
        this.router.navigate(['/chat']);
        this.alerts.setMessage(response.message, 'success');
      } else {
        this.alerts.setMessage(response.message, 'error');
      }
    }).catch((error) => {
      this.alerts.setMessage('Etwas ist schief gelaufen', 'error');
    });
  }

}
